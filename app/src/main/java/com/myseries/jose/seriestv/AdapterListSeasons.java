package com.myseries.jose.seriestv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by Jose on 29/04/2018.
 */

public class AdapterListSeasons extends ArrayAdapter<Temporada> {

    private ArrayList<Temporada> seasons;
    public AdapterListSeasons(Context context,  ArrayList<Temporada> seasons){
        super(context, R.layout.layout_item_seasons, seasons);
        this.seasons = seasons;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView;
        // Si convertView es null creamos la nueva view inflando el xml
        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            itemView = inflater.inflate(R.layout.layout_item_seasons, null);
            ViewHolder vh = new ViewHolder();
            vh.titulo = itemView.findViewById(R.id.tvSeason);
            itemView.setTag(vh);
        }else{
            itemView=convertView; //Si no es null reutilizamos la view ya creada
        }

        ViewHolder vh = (ViewHolder)itemView.getTag();
        if (seasons.get(position).getSeason_number() != null)
            if (Utils.idiomaDispositivo().equals("es"))
                vh.titulo.setText("Temporada "+seasons.get(position).getSeason_number().toString());
            else
                vh.titulo.setText("Season "+seasons.get(position).getSeason_number().toString());
        return(itemView);
    }

    // Creamos la clase interna
    class ViewHolder {
        TextView titulo;
    }

}
