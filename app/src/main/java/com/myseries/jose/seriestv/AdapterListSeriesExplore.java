package com.myseries.jose.seriestv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Jose on 29/04/2018.
 */

public class AdapterListSeriesExplore extends RecyclerView.Adapter<AdapterListSeriesExplore.ViewHolder>
implements View.OnClickListener{

    private ArrayList<Serie> series;
    private Context context;
    private View.OnClickListener listener;

    public AdapterListSeriesExplore(Context context){
        this.context = context;
        this.series = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_item_explore, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Serie serie = series.get(position);
        if (serie.getPoster_path() != null) {
            Picasso.with(context).load(Utils.baseUrlImages + series.get(position).getPoster_path()).into(holder.imagenSerie);
        }

        if (serie.getName() != null)
            holder.titulo.setText(series.get(position).getName().toString());

    }

    public void  adicionarSeries(ArrayList<Serie> dataset) {
        series.addAll(dataset);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return series.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {

        if (listener != null) {
            listener.onClick(v);
        }

    }

    public void eliminarSerie(Serie serie) {
        series.remove(serie);
        notifyDataSetChanged();
    }

    // Creamos la clase interna
    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imagenSerie;
        TextView titulo;

        public ViewHolder(View view) {
            super(view);
            imagenSerie =  view.findViewById(R.id.imagenSerie);
            titulo = view.findViewById(R.id.tvTituloSerie);
        }
    }

}
