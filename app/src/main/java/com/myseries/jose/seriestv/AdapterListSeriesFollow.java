package com.myseries.jose.seriestv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jose on 29/04/2018.
 */

public class AdapterListSeriesFollow extends RecyclerView.Adapter<AdapterListSeriesFollow.ViewHolder>
implements  View.OnClickListener{

    private ArrayList<Serie> series;
    private Context context;
    private View.OnClickListener listener;
    private String diaDeHoy;
    private Date date;

    public AdapterListSeriesFollow(Context context, ArrayList<Serie> series){
        this.context = context;
        this.series = series;
        date = new Date();
        diaDeHoy = new SimpleDateFormat("dd MMM yyyy").format(date);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_item_follow, null);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (series.get(position).getPoster_path() != null) {
            Picasso.with(context)
                    .load(Utils.baseUrlImages + series.get(position).getPoster_path())
                    .into(holder.imagenSerie);
        }

        if (series.get(position).getNext_episode_to_air().getAir_date() != null &&
                series.get(position).getNext_episode_to_air().getEpisode_number() != null &&
                series.get(position).getNext_episode_to_air().getSeason_number() != null &&
                series.get(position).getNext_episode_to_air().getName() != null) {
            if (diaDeHoy.equals(series.get(position).getNext_episode_to_air().getAir_date())) {
                holder.airDate.setTextColor(context.getResources().getColor(R.color.colorAccent));
                if (Utils.idiomaDispositivo().equals("es"))
                    holder.airDate.setText("SE TRANSMITE HOY");
                else
                    holder.airDate.setText("AIRS TODAY");
            }
            else {
                holder.airDate.setTextColor(context.getResources().getColor(R.color.colorLetra));
                if (Utils.idiomaDispositivo().equals("es"))
                    holder.airDate.setText("Siguiente el "+series.get(position).getNext_episode_to_air().getAir_date());
                else
                    holder.airDate.setText("Next on "+series.get(position).getNext_episode_to_air().getAir_date());
            }
            holder.titulo.setText(series.get(position).getName().toString().toUpperCase());
            if (Utils.idiomaDispositivo().equals("es"))
                holder.episode.setText("T"+series.get(position).getNext_episode_to_air().getSeason_number()+"-E"+
                        series.get(position).getNext_episode_to_air().getEpisode_number()+" · '"+series.get(position).getNext_episode_to_air().getName()+"'");
            else
                holder.episode.setText("S"+series.get(position).getNext_episode_to_air().getSeason_number()+"-E"+
                        series.get(position).getNext_episode_to_air().getEpisode_number()+" · '"+series.get(position).getNext_episode_to_air().getName()+"'");
        }
        else {
            holder.airDate.setTextColor(context.getResources().getColor(R.color.colorLetra));
            if (series.get(position).getName() != null)
                holder.titulo.setText(series.get(position).getName().toString().toUpperCase());
            if (Utils.idiomaDispositivo().equals("es"))
                holder.airDate.setText("No hay fecha de lanzamiento");
            else
                holder.airDate.setText("No release date");
            holder.episode.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return series.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {

        if (listener != null) {
            listener.onClick(v);
        }

    }

    // Creamos la clase interna
    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imagenSerie;
        TextView titulo;
        TextView airDate;
        TextView episode;

        public ViewHolder(View view) {
            super(view);
            imagenSerie = view.findViewById(R.id.imagenSerie);
            titulo = view.findViewById(R.id.tvTituloSerie);
            airDate = view.findViewById(R.id.tvAirDate);
            episode = view.findViewById(R.id.tvDays);
        }
    }

}
