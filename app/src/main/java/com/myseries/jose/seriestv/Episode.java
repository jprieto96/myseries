package com.myseries.jose.seriestv;

import java.io.Serializable;

class Episode implements Serializable{

    private String name;
    private String air_date;
    private Integer episode_number;
    private Integer season_number;
    private double vote_average;
    private String overview;
    private String still_path;

    public Episode() {

    }

    public Episode(String title, String released, Integer episode, double imdbRating, String plot, String poster) {
        name = title;
        air_date = released;
        episode_number = episode;
        this.vote_average = imdbRating;
        overview = plot;
        still_path = poster;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAir_date() {
        return air_date;
    }

    public void setAir_date(String air_date) {
        this.air_date = air_date;
    }

    public Integer getEpisode_number() {
        return episode_number;
    }

    public void setEpisode_number(Integer episode_number) {
        this.episode_number = episode_number;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getStill_path() {
        return still_path;
    }

    public void setStill_path(String still_path) {
        this.still_path = still_path;
    }

    public Integer getSeason_number() {
        return season_number;
    }

    public void setSeason_number(Integer season_number) {
        this.season_number = season_number;
    }
}
