package com.myseries.jose.seriestv;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class ActivityAllEpisodes extends AppCompatActivity {

    ListView listView;
    Temporada temporada;
    Serie serie;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_episodes);

        mAdView = findViewById(R.id.adView3);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Añadir boton atras
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.lvEpisodes);
        Bundle bundle = getIntent().getExtras();
        temporada = (Temporada) bundle.getSerializable("temporada");
        serie = (Serie) bundle.getSerializable("serie");
        setTitle(serie.getName());

        if (!temporada.getEpisodes().isEmpty())
            listView.setAdapter(new AdapterListEpisodes(getApplicationContext(), temporada.getEpisodes()));
        else {
            if (Utils.idiomaDispositivo().equals("es"))
                Toast.makeText(getApplicationContext(), "No hay episodios disponibles para esta temporada", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getApplicationContext(), "No episodes available this season", Toast.LENGTH_LONG).show();
            volverAtras();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                com.myseries.jose.seriestv.AlertDialog alertDialog = new com.myseries.jose.seriestv.AlertDialog(ActivityAllEpisodes.this);
                alertDialog.setTitulo(temporada.getEpisodes().get(i).getName());
                alertDialog.setDescripcion(temporada.getEpisodes().get(i).getAir_date()+"   -   "+temporada.getEpisodes().get(i).getVote_average());
                alertDialog.setPlot(temporada.getEpisodes().get(i).getOverview());
                alertDialog.setPosterCapitulo(Utils.baseUrlImages + temporada.getEpisodes().get(i).getStill_path(), getApplicationContext());
                alertDialog.mostrar();
            }
        });
    }

    private void volverAtras() {
        Intent intent = new Intent(ActivityAllEpisodes.this, ActivitySerie.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("serie", serie);
        intent.putExtras(bundle);
        startActivity(intent);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            volverAtras();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        volverAtras();
    }
}
