package com.myseries.jose.seriestv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterListSeriesBuscar extends RecyclerView.Adapter<AdapterListSeriesBuscar.ViewHolder>
        implements View.OnClickListener{

    private ArrayList<Serie> series;
    private Context context;
    private View.OnClickListener listener;

    public AdapterListSeriesBuscar(Context context, ArrayList<Serie> series){
        this.context = context;
        this.series = series;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_item_explore, parent, false);
        view.setOnClickListener(this);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Serie serie = series.get(position);
        if (serie.getPoster_path() != null) {
            Picasso.with(context).load(Utils.baseUrlImages + series.get(position).getPoster_path()).into(holder.imagenSerie);
        }

        if (serie.getName() != null)
            holder.titulo.setText(series.get(position).getName().toString());

    }

    @Override
    public int getItemCount() {
        return series.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {

        if (listener != null) {
            listener.onClick(v);
        }

    }

    // Creamos la clase interna
    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imagenSerie;
        TextView titulo;

        public ViewHolder(View view) {
            super(view);
            imagenSerie =  view.findViewById(R.id.imagenSerie);
            titulo = view.findViewById(R.id.tvTituloSerie);
        }
    }

}
