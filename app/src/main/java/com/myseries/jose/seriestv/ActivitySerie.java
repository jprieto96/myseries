package com.myseries.jose.seriestv;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ArrowKeyMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ActivitySerie extends AppCompatActivity {

    ImageView imgSerie;
    TextView datos;
    TextView descripcion;
    Serie serie;
    ListView listView;
    Button button;
    ProgressBar progressBar;
    boolean follow;
    String overviewRecortada;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serie);

        mAdView = findViewById(R.id.adView5);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Añadir boton atras
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        serie = (Serie) bundle.getSerializable("serie");
        setTitle(serie.getName());
        imgSerie = findViewById(R.id.imgSerie);
        datos = findViewById(R.id.tvData);
        progressBar = findViewById(R.id.progressBarCargaTemporadas);

        descripcion = findViewById(R.id.tvPlot);
        button = findViewById(R.id.buttonFollow);
        listView = findViewById(R.id.listViewSeeasons);

        if (serie.getOverview() != null) {
            if (serie.getOverview().length() <= 150)
                descripcion.setText(serie.getOverview());
            else {
                overviewRecortada = serie.getOverview().substring(0, 151);
                overviewRecortada += "...";
                descripcion.setText(overviewRecortada);
                descripcion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (descripcion.getText().toString().equals(serie.getOverview())) {
                            button.setVisibility(View.VISIBLE);
                            descripcion.setMovementMethod(new ArrowKeyMovementMethod());
                            descripcion.setText(overviewRecortada);
                        }
                        else if (descripcion.getText().toString().equals(overviewRecortada)) {
                            button.setVisibility(View.GONE);
                            descripcion.setMovementMethod(new ScrollingMovementMethod());
                            descripcion.setText(serie.getOverview());
                        }
                    }
                });
            }
        }

        if (serie.getAllGenres().isEmpty() || serie.getAllGenres() == null)
            datos.setText(String.valueOf(serie.getVote_average()));
        else
            datos.setText(serie.getVote_average() + "   ·   " + serie.getAllGenres());

        if (serie.getPoster_path() != null) {
            Picasso.with(getApplicationContext()).load(Utils.baseUrlImages + serie.getPoster_path()).into(imgSerie);
        }

        if (ifFollow(serie)) {
            if (Utils.idiomaDispositivo().equals("es"))
                button.setText("Siguiendo");
            else
                button.setText("Following");
        }
        else {
            if (Utils.idiomaDispositivo().equals("es"))
                button.setText("Seguir");
            else
                button.setText("Follow");
        }

        CargaTemporadas cargaTemporadas = new CargaTemporadas();
        cargaTemporadas.execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ActivitySerie.this, ActivityAllEpisodes.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("serie", serie);
                bundle.putSerializable("temporada", serie.getTemporadas().get(i));
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ifFollow(serie)) {
                   seguir(serie);
                }
                else {
                    dejarDeSeguir(serie);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(ActivitySerie.this, TabFollowing.class);
            startActivity(intent);
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

   @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ActivitySerie.this, TabFollowing.class);
        startActivity(intent);
        this.finish();
    }

    public void seguir(Serie serie) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),

                "bb_dd_ms", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        ContentValues registro = new ContentValues();

        registro.put("id", serie.getId());
        registro.put("Title", serie.getName());
        registro.put("Genre", serie.getAllGenres());
        registro.put("Plot", serie.getOverview());
        registro.put("Poster", serie.getPoster_path());
        registro.put("Released", serie.getFirst_air_date());
        registro.put("Rating", serie.getVote_average());
        registro.put("totalSeasons", serie.getNumber_of_seasons());

        bd.insert("serie", null, registro);

        Cursor fila = bd.rawQuery(

                Utils.select_all_series, null);

        while (fila.moveToNext()) {
            if (serie.getName().equals(fila.getString(1)))
                serie.setId(fila.getInt(0));
        }

        bd.close();

        if (Utils.idiomaDispositivo().equals("es"))
            button.setText("Siguiendo");
        else
            button.setText("Following");


    }

    public void dejarDeSeguir(Serie serie) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "bb_dd_ms", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        int cant = bd.delete("serie", "id = "+serie.getId(), null);

        bd.close();

        if (cant == 1) {

            if (Utils.idiomaDispositivo().equals("es"))
                button.setText("Seguir");
            else
                button.setText("Follow");

        }else

            Toast.makeText(this, "Error",

                    Toast.LENGTH_LONG).show();
    }

    class CargaTemporadas extends AsyncTask<Void, Void, ArrayList<Temporada>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        @Override
        protected ArrayList<Temporada> doInBackground(Void... voids) {
            Gson gson = new GsonBuilder().create();
            serie.getTemporadas().clear();
            if (serie.getNumber_of_seasons() != null) {
                for (int i = 1; i <= serie.getNumber_of_seasons(); i++) {
                    try {
                        String respuestaTemporada = HttpRequest.get(Utils.baseUrl + serie.getId() + "/season/" + i, true, "api_key", Utils.apikey, "language", Utils.idiomaDispositivo())
                                .acceptJson()
                                .body();
                        Temporada temporada = gson.fromJson(respuestaTemporada, Temporada.class);
                        serie.getTemporadas().add(temporada);
                    }
                    catch (Exception e) {
                        Handler handler =  new Handler(getApplicationContext().getMainLooper());
                        handler.post( new Runnable(){
                            public void run(){
                                Utils.errorInternet(getApplicationContext());
                            }
                        });
                    }
                }
            }
            return serie.getTemporadas();
        }

        @Override
        protected void onPostExecute(ArrayList<Temporada> temporadas) {
            if (temporadas.size() > 0) {
                progressBar.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                listView.setAdapter(new AdapterListSeasons(getApplicationContext(), temporadas));
            }
            else {
                Toast.makeText(getApplicationContext(), "No seasons", Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean ifFollow(Serie serie) {

        boolean follow = false;
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "bb_dd_ms", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery(

                "select * from serie where id = "+serie.getId() , null);

        if ( fila.moveToFirst() ) {
            follow = true;
        }

        bd.close();

        return follow;

    }

}
