package com.myseries.jose.seriestv;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Jose on 29/04/2018.
 */

public class Serie implements Serializable{

    private Integer id;
    private String name;
    private String first_air_date;
    private String overview;
    private String poster_path;
    private double vote_average;
    private ArrayList<Genero> genres;
    private String allGenres;
    private Integer number_of_seasons;
    private ArrayList<Temporada> temporadas;
    private boolean notificada;
    private String airDateAnterior;
    private Episode next_episode_to_air = new Episode();

    public Serie() {
        temporadas = new ArrayList<>();
        notificada = false;
    }

    public Serie(Integer id, String title, String genero, String plot, String poster, String released, double imdbRating, Integer totalSeasons) {
        this.id = id;
        name = title;
        first_air_date = released;
        overview = plot;
        poster_path = poster;
        this.vote_average = imdbRating;
        this.allGenres = genero;
        this.number_of_seasons = totalSeasons;
        temporadas = new ArrayList<>();
        notificada = false;
    }

    public void inicializarSerieDeNuevo(String name, String plot, ArrayList<Genero> generos){
        this.name = name;
        this.overview = plot;
        this.allGenres = Utils.concatenarGeneros(generos);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public ArrayList<Genero> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genero> genres) {
        this.genres = genres;
    }

    public String getAllGenres() {
        return allGenres;
    }

    public void setAllGenres(String allGenres) {
        this.allGenres = allGenres;
    }

    public Integer getNumber_of_seasons() {
        return number_of_seasons;
    }

    public void setNumber_of_seasons(Integer number_of_seasons) {
        this.number_of_seasons = number_of_seasons;
    }

    public ArrayList<Temporada> getTemporadas() {
        return temporadas;
    }

    public void setTemporadas(ArrayList<Temporada> temporadas) {
        this.temporadas = temporadas;
    }

    public Episode getNext_episode_to_air() {
        return next_episode_to_air;
    }

    public void setNext_episode_to_air(Episode next_episode_to_air) {
        this.next_episode_to_air = next_episode_to_air;
    }

    public boolean isNotificada() {
        return notificada;
    }

    public void setNotificada(boolean notificada) {
        this.notificada = notificada;
    }

    public String getAirDateAnterior() {
        return airDateAnterior;
    }

    public void setAirDateAnterior(String airDateAnterior) {
        this.airDateAnterior = airDateAnterior;
    }
}
