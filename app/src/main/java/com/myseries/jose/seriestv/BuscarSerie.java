package com.myseries.jose.seriestv;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class BuscarSerie extends AppCompatActivity {

    ArrayList<Serie> seriesFavs;
    EditText editText;
    ArrayList<Serie> series;
    FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;
    RecyclerView recyclerView2;
    TextView textView;
    ProgressBar progressBar;
    AdapterListSeriesBuscar adapterListSeriesExplore;
    AdapterListSeriesBuscar adapterListSeriesExplore2;
    GridLayoutManager gridLayoutManager;
    GridLayoutManager gridLayoutManager2;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_serie);

        mAdView = findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Utils.id_intersticial);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                CargarSerieBuscada cargarSerieBuscada = new CargarSerieBuscada();
                cargarSerieBuscada.execute(editText.getText().toString());
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });

        // Añadir boton atras
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView = findViewById(R.id.tvInfo);
        recyclerView = findViewById(R.id.listaSeriesBuscadas);
        recyclerView2 = findViewById(R.id.listaSeriesBuscadas2);
        gridLayoutManager = new GridLayoutManager(this, 1);
        gridLayoutManager2 = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView2.setLayoutManager(gridLayoutManager2);
        int spanCount = 2;
        int spacing = 40;
        boolean includeEdge = false;
        recyclerView2.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        floatingActionButton = findViewById(R.id.btnSrc);
        editText = findViewById(R.id.editTextBuscar);
        progressBar = findViewById(R.id.progressBarBuscarSerie);
        series = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        seriesFavs = (ArrayList<Serie>) bundle.getSerializable("follow");

        if (Utils.idiomaDispositivo().equals("es")){
            setTitle("Buscar");
            editText.setHint("Escribe un título");
        }
        else {
            setTitle("Search");
            editText.setHint("Type a title");
        }

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText("");
                series.clear();
                if (mInterstitialAd.isLoaded())
                    mInterstitialAd.show();
                else {
                    CargarSerieBuscada cargarSerieBuscada = new CargarSerieBuscada();
                    cargarSerieBuscada.execute(editText.getText().toString());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BuscarSerie.this, TabExplore.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("follow", seriesFavs);
        intent.putExtras(bundle);
        startActivity(intent);
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(BuscarSerie.this, TabExplore.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("follow", seriesFavs);
            intent.putExtras(bundle);
            startActivity(intent);
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    class CargarSerieBuscada extends AsyncTask<String, Void, ArrayList<Serie>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Serie> doInBackground(String... strings) {
            String titulo = strings[0];
            Gson gson = new GsonBuilder().create();
            try {
                String respuesta = HttpRequest.get("https://api.themoviedb.org/3/search/tv",true, "api_key", Utils.apikey, "query", titulo, "language", Utils.idiomaDispositivo())
                        .acceptJson()
                        .body();
                Discover discover = gson.fromJson(respuesta, Discover.class);
                for (Serie s : discover.getSeries()) {
                    String respuesta2 = HttpRequest.get(Utils.baseUrl + s.getId(),true, "api_key", Utils.apikey, "query", titulo, "language", Utils.idiomaDispositivo())
                            .acceptJson()
                            .body();
                    Serie serie = gson.fromJson(respuesta2, Serie.class);
                    if (serie.getGenres() != null)
                        serie.setAllGenres(Utils.concatenarGeneros(serie.getGenres()));
                    if (serie.getName() != null && serie.getId() != null)
                        series.add(serie);
                }
            }
            catch (Exception e) {
                Handler handler =  new Handler(getApplicationContext().getMainLooper());
                handler.post( new Runnable(){
                    public void run(){
                        Utils.errorInternet(getApplicationContext());
                    }
                });
            }

            return series;
        }

        @Override
        protected void onPostExecute(ArrayList<Serie> ser) {
            progressBar.setVisibility(View.GONE);
            if (ser.isEmpty()) {
                recyclerView.setVisibility(View.GONE);
                textView.setText("Not Found");
                textView.setVisibility(View.VISIBLE);
            }
            else {
                if (ser.size() > 1) {
                    recyclerView.setVisibility(View.GONE);
                    recyclerView2.setLayoutManager(gridLayoutManager2);
                    recyclerView2.setVisibility(View.VISIBLE);
                    adapterListSeriesExplore2 = new AdapterListSeriesBuscar(getApplicationContext(), ser);
                    recyclerView2.setAdapter(adapterListSeriesExplore2);
                    adapterListSeriesExplore2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(BuscarSerie.this, ActivitySerie.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("serie", series.get(recyclerView2.getChildAdapterPosition(v)));
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
                else {
                    recyclerView2.setVisibility(View.GONE);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setVisibility(View.VISIBLE);
                    adapterListSeriesExplore = new AdapterListSeriesBuscar(getApplicationContext(), ser);
                    recyclerView.setAdapter(adapterListSeriesExplore);
                    adapterListSeriesExplore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(BuscarSerie.this, ActivitySerie.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("serie", series.get(recyclerView.getChildAdapterPosition(v)));
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    });
                }

            }
        }
    }
}
