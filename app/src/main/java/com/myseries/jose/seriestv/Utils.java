package com.myseries.jose.seriestv;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class Utils {

    public static  final String Crear_Bd_Serie = "create table serie(id integer primary key , Title text, Genre text, Plot text," +
            " Poster text, Released text, Rating double, totalSeasons integer, nameNextEpisode text, seasonNextEpisode integer, nextEpisode integer, dateNextEpisode text)";
    public static final String select_all_series = "select * from serie";
    public static final String baseUrlImages = "https://image.tmdb.org/t/p/original";
    public static final String baseUrl = "https://api.themoviedb.org/3/tv/";
    public static final String apikey = "a7e2e39465e19f06e247e8972d8e0ca9";
    public static String concatenarGeneros(ArrayList<Genero> generos) {
        String generosConcatenados="";
        if (generos != null) {
            for (int i=0; i<generos.size(); i++) {
                if (i != generos.size()-1)
                    generosConcatenados += generos.get(i).getName() + ", ";
                else
                    generosConcatenados += generos.get(i).getName();
            }
        }
        return generosConcatenados;
    }

    public static String idiomaDispositivo() {
        return Locale.getDefault().getLanguage();
    }

    public static void errorInternet(Context context) {
        if (Utils.idiomaDispositivo().equals("es"))
            Toast.makeText(context, "Por favor revise su conexión a internet", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(context, "Please check your internet conexion", Toast.LENGTH_LONG).show();
    }
    public static final String id_intersticial = "ca-app-pub-6368189761090654/9361482647";
}
