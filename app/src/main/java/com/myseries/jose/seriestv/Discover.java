package com.myseries.jose.seriestv;

import java.util.ArrayList;

public class Discover {

    private ArrayList<Serie> results = new ArrayList<>();

    public ArrayList<Serie> getSeries() {
        return results;
    }

    public void setSeries(ArrayList<Serie> series) {
        this.results = series;
    }
}
