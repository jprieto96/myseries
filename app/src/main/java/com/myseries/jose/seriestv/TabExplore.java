package com.myseries.jose.seriestv;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by Jose on 26/02/2018.
 */

public class TabExplore extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Serie> series;
    ArrayList<Serie> seriesFavs;
    FloatingActionButton floatingActionButton;
    ProgressBar progressBar;
    GridLayoutManager gridLayoutManager;
    ArrayList<Serie> arrayConTodasLasSeries;
    AdapterListSeriesExplore adapterListSeriesExplore;
    Integer paginaSeriesPopulares = 1;
    boolean aptoParaCargar;
    private AdView mAdView;

    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_explore);

        mAdView = findViewById(R.id.adView4);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Añadir boton atras
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.listaSeriesExplore);
        gridLayoutManager = new GridLayoutManager(this, 2);
        int spanCount = 2;
        int spacing = 40;
        boolean includeEdge = false;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recyclerView.setLayoutManager(gridLayoutManager);
        adapterListSeriesExplore = new AdapterListSeriesExplore(getApplicationContext());
        recyclerView.setAdapter(adapterListSeriesExplore);
        progressBar = findViewById(R.id.pbExplore);
        series = new ArrayList<>();
        arrayConTodasLasSeries = new ArrayList<>();
        floatingActionButton = findViewById(R.id.btnSearch);
        Bundle bundle = getIntent().getExtras();
        seriesFavs = (ArrayList<Serie>) bundle.getSerializable("follow");
        aptoParaCargar = true;

        if (Utils.idiomaDispositivo().equals("es"))
            setTitle("Series Populares");
        else
            setTitle("Popular Series");

        CargarSerie cargarSerie = new CargarSerie();
        cargarSerie.execute(paginaSeriesPopulares);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TabExplore.this, BuscarSerie.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("follow", seriesFavs);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = gridLayoutManager.getChildCount();
                    int totalItemCount = gridLayoutManager.getItemCount();
                    int pastVisibleItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (aptoParaCargar) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            paginaSeriesPopulares++;
                            aptoParaCargar = false;
                            CargarSerie cs = new CargarSerie();
                            cs.execute(paginaSeriesPopulares);
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            Intent intent = new Intent(TabExplore.this, TabFollowing.class);
            startActivity(intent);
            TabExplore.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(TabExplore.this, TabFollowing.class);
        startActivity(intent);
        TabExplore.this.finish();
    }

    class CargarSerie extends AsyncTask<Integer, Void, ArrayList<Serie>> {

        @Override
        protected void onPreExecute() {
            if (paginaSeriesPopulares.equals(1)) {
                recyclerView.setVisibility(View.GONE);
            }
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Serie> doInBackground(Integer... integers) {
            series.clear();
            Gson gson = new GsonBuilder().create();
            Integer page = integers[0];
            try {
                String respuesta = HttpRequest.get("https://api.themoviedb.org/3/discover/tv",true, "api_key", Utils.apikey, "sort_by", "popularity.desc", "page", page, "language", Utils.idiomaDispositivo())
                        .acceptJson()
                        .body();
                if (respuesta != null) {
                    Discover discover = gson.fromJson(respuesta, Discover.class);
                    for (Serie s : discover.getSeries()) {
                        String respuesta2 = HttpRequest.get(Utils.baseUrl + s.getId(),true, "api_key", Utils.apikey, "language", Utils.idiomaDispositivo())
                                .acceptJson()
                                .body();
                        if (respuesta2 != null) {
                            Serie serie = gson.fromJson(respuesta2, Serie.class);
                            serie.setAllGenres(Utils.concatenarGeneros(serie.getGenres()));
                            series.add(serie);
                        }
                    }
                }
            }
            catch (Exception e) {
                Handler handler =  new Handler(getApplicationContext().getMainLooper());
                handler.post( new Runnable(){
                    public void run(){
                        Utils.errorInternet(getApplicationContext());
                    }
                });
                Intent intent = new Intent(TabExplore.this, TabFollowing.class);
                startActivity(intent);
                TabExplore.this.finish();
            }

            for (Serie serie : seriesFavs) {
                for(int i=0; i<series.size(); i++) {
                    if (serie.getId().equals(series.get(i).getId()))
                        series.remove(i);
                }
            }
            arrayConTodasLasSeries.addAll(series);
            return series;
        }

        @Override
        protected void onPostExecute(ArrayList<Serie> ser) {
            if (paginaSeriesPopulares.equals(1)) {
                recyclerView.setVisibility(View.VISIBLE);
                if (seriesFavs.isEmpty()) {
                    if (Utils.idiomaDispositivo().equals("es"))
                        Toast.makeText(getApplicationContext(), "Añade una serie a tus favoritas", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(getApplicationContext(), "Add one to your favourites", Toast.LENGTH_LONG).show();
                }
            }

            progressBar.setVisibility(View.GONE);
            adapterListSeriesExplore.adicionarSeries(ser);
            aptoParaCargar = true;
            floatingActionButton.setVisibility(View.VISIBLE);

            adapterListSeriesExplore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (seriesFavs.size() < 1) {
                        Serie serieParaEliminar = arrayConTodasLasSeries.get(recyclerView.getChildAdapterPosition(v));
                        alta(arrayConTodasLasSeries.get(recyclerView.getChildAdapterPosition(v)));
                        if (Utils.idiomaDispositivo().equals("es"))
                            Toast.makeText(getApplicationContext(), "Añadida", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_LONG).show();
                        seriesFavs.add(arrayConTodasLasSeries.get(recyclerView.getChildAdapterPosition(v)));
                        adapterListSeriesExplore.eliminarSerie(serieParaEliminar);
                        arrayConTodasLasSeries.remove(serieParaEliminar);
                    }
                    else {
                        Intent intent = new Intent(TabExplore.this, ActivitySerie.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("serie", arrayConTodasLasSeries.get(recyclerView.getChildAdapterPosition(v)));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
    }

    public void alta(Serie serie) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(getApplicationContext(),

                "bb_dd_ms", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        ContentValues registro = new ContentValues();

        registro.put("id", serie.getId());
        registro.put("Title", serie.getName());
        registro.put("Genre", serie.getAllGenres());
        registro.put("Plot", serie.getOverview());
        registro.put("Poster", serie.getPoster_path());
        registro.put("Released", serie.getFirst_air_date());
        registro.put("Rating", serie.getVote_average());
        registro.put("totalSeasons", serie.getNumber_of_seasons());

        bd.insert("serie", null, registro);

        bd.close();

    }
}

