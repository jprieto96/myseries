package com.myseries.jose.seriestv;

import android.app.Dialog;
import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class AlertDialog {

    TextView titulo;
    TextView descripcion;
    Dialog dialogo;
    ImageView posterCapitulo;
    TextView plot;

    public AlertDialog(Context context) {
        dialogo = new Dialog(context);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(true);
        dialogo.setContentView(R.layout.alert_dialog);

        titulo = dialogo.findViewById(R.id.tvTitleDialog);
        descripcion = dialogo.findViewById(R.id.tvDescDialog);
        posterCapitulo = dialogo.findViewById(R.id.posterCapitulo);
        plot = dialogo.findViewById(R.id.tvPlotDialog);
        plot.setMovementMethod(new ScrollingMovementMethod());
    }

    public void mostrar() {
        dialogo.show();
    }

    public void setTitulo(String titulo) {
        this.titulo.setText(titulo);
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.setText(descripcion);
    }

    public void setPosterCapitulo(String posterCapitulo, Context context) {
        if (posterCapitulo != null) {
            Picasso.with(context).load(posterCapitulo).into(this.posterCapitulo);
        }
    }

    public void setPlot(String plot) {
        this.plot.setText(plot);
    }

    public TextView getPlot() {
        return plot;
    }
}
