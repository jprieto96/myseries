package com.myseries.jose.seriestv;

import java.io.Serializable;
import java.util.ArrayList;

class Temporada implements Serializable{

    private String name;
    private String season_number;
    private ArrayList<Episode> episodes;

    public Temporada() {

    }

    public Temporada(String title, String season, ArrayList<Episode> Episodes) {
        name = title;
        season_number = season;
        this.episodes = Episodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason_number() {
        return season_number;
    }

    public void setSeason_number(String season_number) {
        this.season_number = season_number;
    }

    public ArrayList<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(ArrayList<Episode> episodes) {
        this.episodes = episodes;
    }
}
