package com.myseries.jose.seriestv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jose on 29/04/2018.
 */

public class AdapterListEpisodes extends ArrayAdapter<Episode> {

    private ArrayList<Episode> episodes;
    public AdapterListEpisodes(Context context, ArrayList<Episode> episodes){
        super(context, R.layout.layout_item_seasons, episodes);
        this.episodes = episodes;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView;
        // Si convertView es null creamos la nueva view inflando el xml
        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            itemView = inflater.inflate(R.layout.layout_item_seasons, null);
            ViewHolder vh = new ViewHolder();
            vh.titulo = itemView.findViewById(R.id.tvSeason);
            itemView.setTag(vh);
        }else{
            itemView=convertView; //Si no es null reutilizamos la view ya creada
        }

        ViewHolder vh = (ViewHolder)itemView.getTag();
        vh.titulo.setText(episodes.get(position).getEpisode_number().toString()+"   -   "+episodes.get(position).getName().toString());
        return(itemView);
    }

    // Creamos la clase interna
    class ViewHolder {
        TextView titulo;
    }

}
