package com.myseries.jose.seriestv;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Date;

/**
 * Created by Jose on 26/02/2018.
 */

public class TabFollowing extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Serie> series;
    ArrayList<Serie> seriesConTodosLosDatos;
    FloatingActionButton floatingActionButton;
    GridLayoutManager gridLayoutManager;
    AdapterListSeriesFollow adapterListSeriesFollow;
    Date date;
    Date fechaNextEpisode;
    ProgressBar progressBar;
    private AdView mAdView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_following);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        recyclerView = findViewById(R.id.listaSeriesFollowing);
        progressBar = findViewById(R.id.progressBarFollowing);
        gridLayoutManager = new GridLayoutManager(this, 1);
        int spanCount = 1;
        int spacing = 40;
        boolean includeEdge = false;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        recyclerView.setLayoutManager(gridLayoutManager);
        series = new ArrayList<>();
        seriesConTodosLosDatos = new ArrayList<>();
        floatingActionButton = findViewById(R.id.btnAddFav);

        if (Utils.idiomaDispositivo().equals("es"))
            setTitle("Siguiendo");
        else
            setTitle("Following");

        consultaSeries();
        date = new Date();
        fechaNextEpisode = null;

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TabFollowing.this, TabExplore.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("follow", seriesConTodosLosDatos);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

    }

    public void consultaSeries() {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "bb_dd_ms", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery(

                Utils.select_all_series, null);

        while (fila.moveToNext()) {
            Serie serie = new Serie(fila.getInt(0), fila.getString(1), fila.getString(2), fila.getString(3),
                    fila.getString(4), fila.getString(5), fila.getDouble(6), fila.getInt(7));
            series.add(serie);
        }

        if (series.isEmpty()) {
            Intent intent = new Intent(TabFollowing.this, TabExplore.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("follow", seriesConTodosLosDatos);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
        else {
            Collections.reverse(series);
        }

        CargarNextEpisode cargarNextEpisode = new CargarNextEpisode();
        cargarNextEpisode.execute(series);

        bd.close();

    }

    class CargarNextEpisode extends AsyncTask<ArrayList<Serie>, Void, ArrayList<Serie>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            floatingActionButton.setVisibility(View.GONE);
        }

        @Override
        protected ArrayList<Serie> doInBackground(ArrayList<Serie>... series) {
            Gson gson = new GsonBuilder().create();
            ArrayList<Serie> series1 = series[0];
            for (Serie s : series1) {
                try {
                    String respuesta = HttpRequest.get(Utils.baseUrl + s.getId(), true, "api_key", Utils.apikey, "language", Utils.idiomaDispositivo())
                            .acceptJson()
                            .body();
                    if (respuesta != null) {
                        Serie se = gson.fromJson(respuesta, Serie.class);
                        if (se.getNext_episode_to_air() != null)
                            s.setNext_episode_to_air(se.getNext_episode_to_air());

                        if (s.getNext_episode_to_air() != null && s.getNext_episode_to_air().getAir_date() != null) {
                            SimpleDateFormat parseador = new SimpleDateFormat("yyyy-MM-dd");
                            SimpleDateFormat formateador = new SimpleDateFormat("dd MMM yyyy");
                            try {
                                fechaNextEpisode = parseador.parse(s.getNext_episode_to_air().getAir_date());
                                String fechaAMostrar = formateador.format(fechaNextEpisode);
                                s.getNext_episode_to_air().setAir_date(fechaAMostrar);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        s.inicializarSerieDeNuevo(se.getName(), se.getOverview(), se.getGenres());
                    }
                }
                catch(Exception e) {
                    Handler handler =  new Handler(getApplicationContext().getMainLooper());
                    handler.post( new Runnable(){
                        public void run(){
                            Utils.errorInternet(getApplicationContext());
                        }
                    });
                }
            }
            return series1;
        }

        @Override
        protected void onPostExecute(final ArrayList<Serie> series) {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            floatingActionButton.setVisibility(View.VISIBLE);
            seriesConTodosLosDatos = series;
            adapterListSeriesFollow = new AdapterListSeriesFollow(getApplicationContext(), seriesConTodosLosDatos);
            if (seriesConTodosLosDatos.size() > 0) {
                recyclerView.setAdapter(adapterListSeriesFollow);
                adapterListSeriesFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TabFollowing.this, ActivitySerie.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("serie", seriesConTodosLosDatos.get(recyclerView.getChildAdapterPosition(v)));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        TabFollowing.this.finish();
                    }
                });
            }
            else {
                Intent intent = new Intent(TabFollowing.this, TabExplore.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("follow", seriesConTodosLosDatos);
                intent.putExtras(bundle);
                startActivity(intent);
                TabFollowing.this.finish();
            }
        }
    }
}
